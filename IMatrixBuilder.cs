﻿using System;
using System.Collections.Generic;

namespace Dobeshy
{
    public interface IMatrixBuilder<T> where T : struct
    {
        T[,] BuildMatrix(int N);
        List<T> MultiplyMatrix(T[,] matrix, List<short> stream, int start);
        T[,] GetTransponentMatrix(T[,] mx);
    }
}
