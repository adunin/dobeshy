﻿using System;
using System.Collections.Generic;

namespace Dobeshy
{
    public class FloatMatrixBuilder
    {
        public static float DefaultC1 = (float)(0.6830127 / Math.Sqrt(2.0));
        public static float DefaultC2 = (float)(1.1830127 / Math.Sqrt(2.0));
        public static float DefaultC3 = (float)(0.3169873 / Math.Sqrt(2.0));
        public static float DefaultC4 = (float)(-0.1830127 / Math.Sqrt(2.0));
        private float c1 = float.NaN;
        private float c2 = float.NaN;
        private float c3 = float.NaN;
        private float c4 = float.NaN;

        public FloatMatrixBuilder(float c1, float c2, float c3, float c4)
        {
            this.c1 = c1;
            this.c2 = c2;
            this.c3 = c3;
            this.c4 = c4;

            this.CreateBaseMatrix();
        }

        private float[,] Matrix;

        private void CreateBaseMatrix()
        {
            this.Matrix = new float[,]
            {
                {c1, c2, c3, c4},
                {c4, -c3, c2, -c1}
            };
        }

        public float[,] BuildMatrix(int N)
        {
            if(!CheckN(N))
                return null;

            var ans = new float[N, N];

            for(int firstPos = 0; firstPos < N; firstPos += 2)
            {
                for(int i = 0; i < 2; i++)
                {
                    for(int j = 0; j < 4; j++)
                    {
                        ans[firstPos + i, (firstPos + j) % N] = Matrix[i, j];
                    }
                }
            }

            return ans;
        }

        private bool CheckN(int N)
        {
            if(N % 2 == 1)
                return false;
            return true;
        }

        public List<float> MultiplyMatrix(float[,] matrix, List<short> stream, int start)
        {
            var ans = new List<float>();

            for(var i = 0; i < matrix.GetLength(0); i++)
            {
                float tmp = 0;
                for(var j = 0; j < matrix.GetLength(1); j++)
                {
                    tmp += (float)(stream[i + start] * matrix[j, i]);
                }

                ans.Add(tmp);
            }

            return ans;
        }

        /// <summary>
        /// Перемножает <see cref="matrix"/> на <see cref="stream"/>. Из <see cref="stream"/> берется последовательность
        /// дляно равной измерению матрицы начиная с позиции <see cref="stream"/>.
        /// </summary>
        /// <param name="matrix">Матрица.</param>
        /// <param name="stream">Вектор.</param>
        /// <param name="start">Начальная позиция в векторе.</param>
        /// <returns></returns>
        public List<float> MultiplyMatrix(float[,] matrix, List<float> stream, int start)
        {
            var ans = new List<float>();

            for(var i = 0; i < matrix.GetLength(0); i++)
            {
                float tmp = 0;
                for(var j = 0; j < matrix.GetLength(0); j++)
                {
                    tmp += stream[j + start] * ((float)(matrix[i, j]));
                }

                ans.Add(tmp);
            }

            return ans;
        }

        public float[,] GetTransponentMatrix(float[,] mx)
        {
            var len = mx.GetLength(0);
            var ans = new float[len, len];

            for(int i = 0; i < len; i++)
            {
                for(int j = 0; j < len; j++)
                {
                    ans[i, j] = mx[j, i];
                }
            }

            return ans;
        }
    }
}
