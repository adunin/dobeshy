﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Dobeshy.Model;
using NAudio.Wave;

namespace Dobeshy
{
    public class ShortStrategy : IStrategy
    {
        private const string fileName = "Output.mp3";

        private short c1 = short.MinValue;
        private short c2 = short.MinValue;
        private short c3 = short.MinValue;
        private short c4 = short.MinValue;

        private int mxSize = 4;
        private FileHelper fileHelper;
        private short mod;
        private const int StepDivider = 2;
        private const int MinMatrixSize = 4;

        public void Execute()
        {

        }

        #region Init and Parse
        public bool Init(string textSize, string textC1, string textC2, string textC3, string textC4, string textModule, FileHelper fileHelper)
        {
            bool isSuccess = true;

            if (!int.TryParse(textSize, out mxSize))
            {
                MessageBox.Show("Не удалось получить размер матрицы.");
                return false;
            }

            if (!short.TryParse(textModule, out mod))
            {
                MessageBox.Show("Не удалось получить основание поля.");
                return false;
            }

            isSuccess &= Parse(textC1, out c1);
            if (!isSuccess)
                return false;

            isSuccess &= Parse(textC2, out c2);
            if (!isSuccess)
                return false;

            isSuccess &= Parse(textC3, out c3);
            if (!isSuccess)
                return false;

            isSuccess &= Parse(textC4, out c4);
            if (!isSuccess)
                return false;

            this.fileHelper = fileHelper;

            return true;
        }

        private bool Parse(string str, out short value)
        {
            value = 0;
            if (String.IsNullOrEmpty(str))
            {
                MessageBox.Show("Не все коэффициенты заданы");
                return false;
            }

            var result = short.TryParse(str, out value);
            return result;
        }
        #endregion

        public void DoWorkHandler(object obj, DoWorkEventArgs args)
        {
            var worker = obj as BackgroundWorker;
            if (worker == null)
                throw new ApplicationException("Непоправимая ошибка.");
            worker.ReportProgress(0);

            Solve(worker, args);
        }

        private void Solve(BackgroundWorker worker, DoWorkEventArgs args)
        {
            List<short> intStream = fileHelper.GetIntStream();
            List<bool> signs = intStream.Select(el => el > 0).ToList();

            SeparatedData<short> outstream = MakeForward(worker, args, intStream);
            List<short> stream2 = MakeBackward(worker, args, outstream);
            outstream.Dispose();

            for (int i = 0; i < stream2.Count; i++)
            {
                stream2[i] = (short)(signs[i] ? ((stream2[i] + mod) % mod) : (stream2[i] - mod) % mod);
            }

            worker.ReportProgress(95);
            stream2 = stream2.Select(el => (short)(el / 2)).ToList();

            var writer = new WaveFileWriter(Path.Combine(fileHelper.Directory, fileName), fileHelper.inputFormat);
            writer.WriteSamples(stream2.ToArray(), 0, stream2.Count);
            writer.Flush();
            writer.Close();

            worker.ReportProgress(100);
        }

        public SeparatedData<short> MakeForward(BackgroundWorker worker, DoWorkEventArgs args, List<short> stream)
        {
            var matrixBuilder = new IntMatrixBuilder(c1, c2, c3, c4, mod);
            SeparatedData<short> outstream = new SeparatedData<short>();
            List<short> currentStream = stream;

            for (int i = mxSize, conversationSetp = 0; i >= MinMatrixSize; i /= StepDivider, conversationSetp++)
            {
                short[,] matrix = matrixBuilder.BuildMatrix(i);

                // Проверка отмены
                if (worker.CancellationPending)
                {
                    args.Cancel = true;
                    return outstream;
                }

                List<short> forwardStream, outUp, outDown;
                bool isCanceled = GetForwardStream(worker, args, currentStream, matrixBuilder, matrix, out forwardStream, out outUp, out outDown);
                outstream.Add(outUp, outDown);
                currentStream = outstream[conversationSetp].Key;

                if (isCanceled)
                    return outstream;
            }

            return outstream;
        }

        private bool GetForwardStream(BackgroundWorker worker, DoWorkEventArgs args, List<short> streamIn,
            IntMatrixBuilder matrixBuilder, short[,] matrix, out List<short> streamOut, out List<short> streamOutUp, out List<short> streamOutDown)
        {
            streamOut = new List<short>();
            streamOutUp = new List<short>();
            streamOutDown = new List<short>();

            // Размер матрицы.
            int size = matrix.GetLength(0);

            // TODO: можно вытащить на самый верх, при вызове.
            // Длина входного вектора должна быть кратна размеру матрицы.
            int zeroElementCount = streamIn.Count % size;
            while (zeroElementCount > 0)
            {
                streamIn.Add(0);
                zeroElementCount--;
            }

            for (int i = 0; i * size < streamIn.Count; i++)
            {
                // Проверка отмены
                if (worker != null && args != null && worker.CancellationPending)
                {
                    args.Cancel = true;
                    return true;
                }

                // Позиция, начиная с которой будет браться вектор длины size для умножения.
                int start = i * size < 0 ? 0 : i * size;

                List<short> tmp = matrixBuilder.MultiplyMatrix(matrix, streamIn, start, mod);
                streamOut.AddRange(tmp);

                List<short> tempOutUp;
                List<short> tempOutDown;
                SplitVector(tmp, out tempOutUp, out tempOutDown);

                streamOutUp.AddRange(tempOutUp);
                streamOutDown.AddRange(tempOutDown);
            }

            return false;
        }

        public List<short> MakeBackward(BackgroundWorker worker, DoWorkEventArgs args, SeparatedData<short> stream)
        {
            IntMatrixBuilder matrixBuilder = new IntMatrixBuilder(c1, c2, c3, c4, mod);

            int streamStep = stream.Count;
            List<short> up = stream[streamStep - 1].Key;
            List<short> down = stream[streamStep - 1].Value;
            List<short> inputStream = up;

            for (int i = streamStep - 1, index = 4; i >= 0; i--, index *= 2)
            {
                List<short> output = new List<short>();
                short[,] matrix = matrixBuilder.BuildMatrix(index);
                short[,] tmatrix = matrixBuilder.GetTransponentMatrix(matrix);

                inputStream = MergeStreams(inputStream, stream[i].Value);

                // Проверка отмены
                if (worker.CancellationPending)
                {
                    args.Cancel = true;
                    return new List<short>();
                }

                bool isCanceled = GetBackwardStream(worker, args, inputStream, matrixBuilder, tmatrix, out output);
                inputStream = output;

                if (isCanceled)
                    return output;
            }
            return inputStream;
        }

        private bool GetBackwardStream(BackgroundWorker worker, DoWorkEventArgs args, List<short> stream1,
            IntMatrixBuilder matrixBuilder, short[,] tMatrix, out List<short> stream2)
        {
            stream2 = new List<short>();
            int size = tMatrix.GetLength(0);
            for (int i = 0; i * size < stream1.Count; i++)
            {
                // Проверка отмены
                if (worker.CancellationPending)
                {
                    args.Cancel = true;
                    return true;
                }

                var start = i * size < 0
                    ? 0
                    : i * size;
                var tmp = matrixBuilder.MultiplyMatrix(tMatrix, stream1, start, mod);
                stream2.AddRange(tmp);
            }
            return false;
        }




        private void SplitVector(List<short> stream, out List<short> outUp, out List<short> outDown)
        {
            outUp = new List<short>();
            outDown = new List<short>();

            for (int i = 0; i < stream.Count; i++)
            {
                outUp.Add(stream[i++]);
                outDown.Add(stream[i]);
            }
        }

        private List<short> MergeStreams(List<short> up, List<short> down)
        {
            List<short> result = new List<short>();
            for (int i = 0; i < up.Count; i++)
            {
                result.Add(up[i]);
                result.Add(down[i]);
            }

            return result;
        }
    }
}
