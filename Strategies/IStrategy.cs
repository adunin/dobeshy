﻿using System;
using System.ComponentModel;

namespace Dobeshy
{
    public interface IStrategy
    {
        bool Init(string textSize, string textC1, string textC2, string textC3, string textC4, string textModule, FileHelper fileHelper);
        void DoWorkHandler(object obj, DoWorkEventArgs args);
    }
}
