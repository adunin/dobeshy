﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Dobeshy.Model;
using NAudio.Wave;

namespace Dobeshy
{
    public class FloatStrategy : IStrategy
    {
        private const string fileName = "Output.mp3";

        private float c1 = float.NaN;
        private float c2 = float.NaN;
        private float c3 = float.NaN;
        private float c4 = float.NaN;

        private int mxSize = 4;
        private FileHelper fileHelper;
        private const int StepDivider = 2;
        private const int MinMatrixSize = 4;

        public void Execute()
        {

        }

        public void SetMxSize(int size)
        {
            mxSize = size;
        }

        #region Init and Parse
        public bool Init(float c1, float c2, float c3, float c4)
        {
            this.c1 = c1;
            this.c2 = c2;
            this.c3 = c3;
            this.c4 = c4;

            return true;
        }

        public bool Init(string textSize, string textC1, string textC2, string textC3, string textC4, string textModule, FileHelper fileHelper)
        {
            bool isSuccess = true;

            if(!int.TryParse(textSize, out mxSize))
            {
                MessageBox.Show(@"Не удалось получить размер матрицы.");
                return false;
            }

            isSuccess &= Parse(textC1, out c1);
            if(!isSuccess)
                return false;

            isSuccess &= Parse(textC2, out c2);
            if(!isSuccess)
                return false;

            isSuccess &= Parse(textC3, out c3);
            if(!isSuccess)
                return false;

            isSuccess &= Parse(textC4, out c4);
            if(!isSuccess)
                return false;

            this.fileHelper = fileHelper;

            return true;
        }

        private bool Parse(string str, out float value)
        {
            value = 0;
            if(String.IsNullOrEmpty(str))
            {
                MessageBox.Show(@"Не все коэффициенты заданы");
                return false;
            }

            var result = float.TryParse(str, out value);
            return result;
        }
        #endregion

        public void DoWorkHandler(object obj, DoWorkEventArgs args)
        {
            var worker = obj as BackgroundWorker;
            if(worker == null)
                throw new ApplicationException("Непоправимая ошибка.");
            worker.ReportProgress(0);

            Solve(worker, args);
        }

        private void Solve(BackgroundWorker worker, DoWorkEventArgs args)
        {
            List<float> floatStream = fileHelper.GetFloatStream();

            SeparatedData<float> outstream = MakeForward(worker, args, floatStream);

            List<float> stream2 = MakeBackward(worker, args, outstream);
            outstream.Dispose();

            float X = 2.0f;
            stream2 = stream2.Select(el => el / X).ToList();

            var writer = new WaveFileWriter(Path.Combine(fileHelper.Directory, fileName), fileHelper.inputFormat);
            writer.WriteSamples(stream2.ToArray(), 0, stream2.Count);
            writer.Flush();
            writer.Close();

            worker.ReportProgress(100);
        }

        /// <summary>Прямое преобразование.</summary>
        public SeparatedData<float> MakeForward(BackgroundWorker worker, DoWorkEventArgs args, List<float> stream)
        {
            FloatMatrixBuilder matrixBuilder = new FloatMatrixBuilder(c1, c2, c3, c4);
            SeparatedData<float> outstream = new SeparatedData<float>();
            List<float> currentStream = stream;
            
            for(int i = mxSize, conversationSetp = 0; i >= MinMatrixSize; i /= StepDivider, conversationSetp++)
            {
                float[,] matrix = matrixBuilder.BuildMatrix(i);

                // Проверка отмены
                if(worker.CancellationPending)
                {
                    args.Cancel = true;
                    return outstream;
                }

                List<float> forwardStream, outUp, outDown;
                bool isCanceled = GetForwardStream(worker, args, currentStream, matrixBuilder, matrix, out forwardStream, out outUp, out outDown);
                outstream.Add(outUp, outDown);
                currentStream = outstream[conversationSetp].Key;

                if(isCanceled)
                    return outstream;
            }
            return outstream;
        }

        public List<float> MakeBackward(BackgroundWorker worker, DoWorkEventArgs args, SeparatedData<float> stream)
        {
            FloatMatrixBuilder matrixBuilder = new FloatMatrixBuilder(c1, c2, c3, c4);

            int streamStep = stream.Count;
            List<float> up = stream[streamStep - 1].Key;
            List<float> down = stream[streamStep - 1].Value;
            List<float> inputStream = up;

            for(int i = streamStep - 1, index = 4; i >= 0; i--, index *= 2)
            {
                List<float> output = new List<float>();
                float[,] matrix = matrixBuilder.BuildMatrix(index);
                float[,] tmatrix = matrixBuilder.GetTransponentMatrix(matrix);

                inputStream = MergeStreams(inputStream, stream[i].Value);

                // Проверка отмены
                if(worker.CancellationPending)
                {
                    args.Cancel = true;
                    return new List<float>();
                }

                bool isCanceled = GetBackwardStream(worker, args, inputStream, matrixBuilder, tmatrix, out output);
                inputStream = output;

                if(isCanceled)
                    return output;
            }
            return inputStream;
        }

        private bool GetBackwardStream(BackgroundWorker worker, DoWorkEventArgs args, List<float> stream1,
            FloatMatrixBuilder matrixBuilder, float[,] tMatrix, out List<float> stream2)
        {
            stream2 = new List<float>();
            int size = tMatrix.GetLength(0);
            for(int i = 0; i * size < stream1.Count; i++)
            {
                // Проверка отмены
                if(worker.CancellationPending)
                {
                    args.Cancel = true;
                    return true;
                }

                var start = i * size < 0
                    ? 0
                    : i * size;
                var tmp = matrixBuilder.MultiplyMatrix(tMatrix, stream1, start);
                stream2.AddRange(tmp);
            }
            return false;
        }

        private bool GetForwardStream(BackgroundWorker worker, DoWorkEventArgs args, List<float> streamIn,
            FloatMatrixBuilder matrixBuilder, float[,] matrix, out List<float> streamOut, out List<float> streamOutUp, out List<float> streamOutDown)
        {
            streamOut = new List<float>();
            streamOutUp = new List<float>();
            streamOutDown = new List<float>();

            // Размер матрицы.
            int size = matrix.GetLength(0);

            // TODO: можно вытащить на самый верх, при вызове.
            // Длина входного вектора должна быть кратна размеру матрицы.
            int zeroElementCount = streamIn.Count % size;
            while(zeroElementCount > 0)
            {
                streamIn.Add(0);
                zeroElementCount--;
            }

            for(int i = 0; i * size < streamIn.Count; i++)
            {
                // Проверка отмены
                if(worker != null && args != null && worker.CancellationPending)
                {
                    args.Cancel = true;
                    return true;
                }

                // Позиция, начиная с которой будет браться вектор длины size для умножения.
                int start = i * size < 0 ? 0 : i * size;

                List<float> tmp = matrixBuilder.MultiplyMatrix(matrix, streamIn, start);
                streamOut.AddRange(tmp);

                List<float> tempOutUp;
                List<float> tempOutDown;
                SplitVector(tmp, out tempOutUp, out tempOutDown);

                streamOutUp.AddRange(tempOutUp);
                streamOutDown.AddRange(tempOutDown);
            }

            return false;
        }

        private void SplitVector(List<float> stream, out List<float> outUp, out List<float> outDown)
        {
            outUp = new List<float>();
            outDown = new List<float>();

            for(int i = 0; i < stream.Count; i++)
            {
                outUp.Add(stream[i++]);
                outDown.Add(stream[i]);
            }
        }

        private List<float> MergeStreams(List<float> up, List<float> down)
        {
            List<float> result = new List<float>();
            for(int i = 0; i < up.Count; i++)
            {
                result.Add(up[i]);
                result.Add(down[i]);
            }

            return result;
        }
    }
}