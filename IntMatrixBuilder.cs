﻿using System;
using System.Collections.Generic;

namespace Dobeshy
{
    public class IntMatrixBuilder
    {
        public static Int16 DefaultC1 = 0;
        public static Int16 DefaultC2 = 0;
        public static Int16 DefaultC3 = 0;
        public static Int16 DefaultC4 = 0;
        private Int16 c1 = 0;
        private Int16 c2 = 0;
        private Int16 c3 = 0;
        private Int16 c4 = 0;

        private Int16 mod = 0;

        public IntMatrixBuilder(Int16 c1, Int16 c2, Int16 c3, Int16 c4, Int16 mod)
        {
            //this.c1 = (short)(c1 < 0 ? c1 + mod : c1);
            //this.c2 = (short)(c2 < 0 ? c2 + mod : c2);
            //this.c3 = (short)(c3 < 0 ? c3 + mod : c3);
            //this.c4 = (short)(c4 < 0 ? c4 + mod : c4);

            this.c1 = c1;
            this.c2 = c2;
            this.c3 = c3;
            this.c4 = c4;

            this.mod = mod;

            this.CreateBaseMatrix();
        }

        private Int16[,] Matrix;

        private void CreateBaseMatrix()
        {
            this.Matrix = new Int16[,]
            {
                {c1, c2, c3, c4},
                //{c4, (Int16)((-c3 + mod)%mod), c2, (Int16)((-c1 + mod)%mod)},
                {c4, (Int16)((mod-c3)%mod), c2, (Int16)((mod-c1)%mod)},




                //{(short)((c1 + mod)%mod), (short)((c2 + mod)%mod), (short)((c3 + mod)%mod), (short)((c4 + mod)%mod)},
                ////{c4, (Int16)(-c3), c2, (Int16)(-c1)},
                //{(short)((c4 + mod)%mod), (Int16)((-c3 + mod)%mod), (short)((c2 + mod)%mod), (Int16)((-c1 + mod)%mod)},
            };
        }

        public Int16[,] BuildMatrix(int N)
        {
            if(!CheckN(N))
                return null;

            var ans = new Int16[N, N];

            for(int firstPos = 0; firstPos < N; firstPos += 2)
            {
                for(int i = 0; i < 2; i++)
                {
                    for(int j = 0; j < 4; j++)
                    {
                        ans[firstPos + i, (firstPos + j) % N] = Matrix[i, j];
                    }
                }
            }

            return ans;
        }

        private bool CheckN(int N)
        {
            if(N % 2 == 1)
                return false;
            return true;
        }

        public List<Int16> MultiplyMatrix(Int16[,] matrix, List<short> stream, int start, short module)
        {
            var ans = new List<Int16>();

            for(var i = 0; i < matrix.GetLength(0); i++)
            {
                Int16 tmp = 0;
                for(var j = 0; j < matrix.GetLength(1); j++)
                {
                    short a1 = (short)((stream[j + start] * matrix[i, j]) % module);
                    tmp = (short)((tmp + a1)%module);
                    //tmp = (short)(a % module);
                }

                ans.Add(tmp);
            }

            return ans;
        }

        public Int16[,] GetTransponentMatrix(Int16[,] mx)
        {
            var len = mx.GetLength(0);
            var ans = new Int16[len, len];

            for(int i = 0; i < len; i++)
            {
                for(int j = 0; j < len; j++)
                {
                    ans[i, j] = mx[j, i];
                }
            }

            return ans;
        }
    }
}
