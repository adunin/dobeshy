﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using NAudio.Wave;

namespace Dobeshy
{
    public class FileHelper
    {
        private readonly OpenFileDialog fileDialog;
        private readonly TextBox pathBox;

        public WaveFormat inputFormat { get; set; }

        public FileHelper(OpenFileDialog fileDialog, TextBox pathBox)
        {
            this.fileDialog = fileDialog;
            this.fileDialog.Filter = "Audio File (*.mp3)|*.mp3;";
            this.pathBox = pathBox;
            this.fileDialog.FileOk += OnSelectFile;
        }

        private void OnSelectFile(object sender, CancelEventArgs e)
        {
            pathBox.Text = fileDialog.FileName;
        }

        public void OpenButton_Click(object sender, EventArgs e)
        {
            this.fileDialog.ShowDialog();
        }

        public string Directory
        {
            get { return Path.GetDirectoryName(fileDialog.FileName); }
        }

        public List<float> GetFloatStream()
        {
            string path = fileDialog.FileName;
            Mp3FileReader mp3 = new Mp3FileReader(path);
            NAudio.Wave.WaveStream pcm = NAudio.Wave.WaveFormatConversionStream.CreatePcmStream(mp3);

            inputFormat = mp3.WaveFormat;

            byte[] buffer = new byte[pcm.Length];
            pcm.Read(buffer, 0, (int)pcm.Length);
            List<float> ans = new List<float>();

            for(var i = 0; (i) * 2 < pcm.Length; i++)
            {
                int offset = i * 2 - 1;
                short sample_1 = BitConverter.ToInt16(buffer, offset > 0 ? offset - 1 : 0);
                float sample = sample_1 / ((float)(Int16.MaxValue));
                ans.Add(sample);
            }

            return ans;
        }

        public List<Int16> GetIntStream()
        {
            var path = fileDialog.FileName;
            var mp3 = new Mp3FileReader(path);
            NAudio.Wave.WaveStream pcm = NAudio.Wave.WaveFormatConversionStream.CreatePcmStream(mp3);

            inputFormat = mp3.WaveFormat;

            byte[] buffer = new byte[pcm.Length];
            pcm.Read(buffer, 0, (int)pcm.Length);
            List<short> ans = new List<Int16>();

            for(var i = 0; (i) * 2 < pcm.Length; i++)
            {
                int offset = i * 2 - 1;
                short sample_1 = BitConverter.ToInt16(buffer, offset > 0 ? offset - 1 : 0);
                short sample = sample_1;
                ans.Add(sample);
            }

            return ans;
        }



    }
}

