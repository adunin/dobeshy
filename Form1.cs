﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using NAudio;
using NAudio.Wave;

namespace Dobeshy
{
    public partial class Form1 : Form
    {
        private FileHelper _fileHelper;
        private string outputPath = @"C:\Temp\2.mp3";

        public static double DefaultC1 = 0.6830127 / Math.Sqrt(2.0);
        public static double DefaultC2 = 1.1830127 / Math.Sqrt(2.0);
        public static double DefaultC3 = 0.3169873 / Math.Sqrt(2.0);
        public static double DefaultC4 = -0.1830127 / Math.Sqrt(2.0);


        public Form1()
        {
            InitializeComponent();

            _fileHelper = new FileHelper(FileDialog, FileTextBox);
            this.OpenButton.Click += this._fileHelper.OpenButton_Click;

            this.ProcessProgressBar.Minimum = 0;
            this.ProcessProgressBar.Maximum = 100;

            this.IsDefaultCheckBox.Checked = true;
            this.IsUseIntCheckBox.Checked = false;
            this.ModuleTextBox.Enabled = false;

            this.SetDefaultValues();
            this.SetAllEnabled(true);
        }

        /// <summary> Устанавливает значения по умолчанию (коэффициенты преобразованияя Добеши) </summary>
        private void SetDefaultValues()
        {
            this.IsUseIntCheckBox.Checked = false;
            this.ModuleTextBox.Enabled = false;
            this.ModuleTextBox.Text = "";

            this.C1TextBox.Text = DefaultC1.ToString("F6");
            this.C2TextBox.Text = DefaultC2.ToString("F6");
            this.C3TextBox.Text = DefaultC3.ToString("F6");
            this.C4TextBox.Text = DefaultC4.ToString("F6");
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            if (Worker != null)
            {
                Worker.CancelAsync();
                this.SetAllEnabled(true);
            }
        }

        private BackgroundWorker Worker;
        private void StartButton_Click(object sender, EventArgs e)
        {
            IStrategy strategy = null;

            if (IsUseIntCheckBox.Checked)
                strategy = new ShortStrategy();
            else
                //strategy = new FloatStrategy();
                strategy = new FloatStrategy();


            var isInited = strategy.Init(MatrixsizeTextBox.Text, C1TextBox.Text, C2TextBox.Text, C3TextBox.Text, C4TextBox.Text, ModuleTextBox.Text, _fileHelper);
            if (!isInited)
                return;

            this.SetAllEnabled(false);

            Worker = new BackgroundWorker();
            Worker.DoWork += strategy.DoWorkHandler;
            Worker.RunWorkerCompleted += ComplitedHandler;
            Worker.ProgressChanged += ProgressChange;
            Worker.WorkerSupportsCancellation = true;
            Worker.WorkerReportsProgress = true;

            Worker.RunWorkerAsync();
        }

        private void ProgressChange(object obj, ProgressChangedEventArgs args)
        {
            ProcessProgressBar.Value = args.ProgressPercentage;
        }

        private void ComplitedHandler(object obj, RunWorkerCompletedEventArgs args)
        {
            if (args.Error != null)
            {
                //throw new Exception("Ошибка");
                MessageBox.Show(args.Error.Message);
                return;
            }

            if (args.Cancelled)
            {
                MessageBox.Show("Операция отменина.");
                this.SetAllEnabled(true);
                return;
            }

            this.SetAllEnabled(true);
            Worker.Dispose();
        }

        private void UseDefaultCheckChanged(object sender, EventArgs e)
        {
            var state = (sender as CheckBox).Checked;

            this.C1TextBox.Enabled = !state;
            this.C2TextBox.Enabled = !state;
            this.C3TextBox.Enabled = !state;
            this.C4TextBox.Enabled = !state;

            if (state)
                this.SetDefaultValues();
            else
                this.ClearMatrixValues();
        }

        private void UseIntCheckChanged(object sender, EventArgs e)
        {
            var isUseIntCheckBox = sender as CheckBox;
            if (isUseIntCheckBox == null)
                return;
            var state = isUseIntCheckBox.Checked;

            if (state)
            {
                IsDefaultCheckBox.Checked = false;
                this.ModuleTextBox.Enabled = true;

                this.ModuleTextBox.Text = "28559";
                this.C1TextBox.Text = "5070";
                this.C2TextBox.Text = "12252";
                this.C3TextBox.Text = "-19265";
                this.C4TextBox.Text = "-26447";

                //this.ModuleTextBox.Text = "23";
                //this.C1TextBox.Text = "2";
                //this.C2TextBox.Text = "9";
                //this.C3TextBox.Text = "-11";
                //this.C4TextBox.Text = "-18";

            }
            else
            {
                this.ModuleTextBox.Enabled = false;
                this.ModuleTextBox.Text = "";
            }
        }

        private void ClearMatrixValues()
        {
            this.C1TextBox.Text = string.Empty;
            this.C2TextBox.Text = string.Empty;
            this.C3TextBox.Text = string.Empty;
            this.C4TextBox.Text = string.Empty;
        }

        #region Matrix chenge size
        private void IncreamentButton_Click(object sender, EventArgs e)
        {
            int currentSize = 0;
            if (!int.TryParse(MatrixsizeTextBox.Text, out currentSize))
            {
                MessageBox.Show("Не удалсь увеличить размер.");
                return;
            }

            currentSize *= 2;

            if (currentSize > 2048)
            {
                MessageBox.Show("Матрица достигла максимального размера!");
                return;
            }
            MatrixsizeTextBox.Text = currentSize.ToString();
        }

        private void DecreamentButton_Click(object sender, EventArgs e)
        {
            int currentSize = 0;
            if (!int.TryParse(MatrixsizeTextBox.Text, out currentSize))
            {
                MessageBox.Show("Не удалсь увеличить размер.");
                return;
            }

            if (currentSize <= 4)
            {
                MessageBox.Show("Матрица достигла минимального размера!");
                return;
            }

            currentSize /= 2;
            MatrixsizeTextBox.Text = currentSize.ToString();
        }
        #endregion

        private void SetAllEnabled(bool value)
        {
            this.OpenFileGroupBox.Enabled = value;
            this.MatrixParamsGroupBox.Enabled = value;

            this.StartButton.Enabled = value;
            this.CancelButton.Enabled = !value;
        }

        private bool GetIntValue(TextBox box, out int value)
        {
            var str = box.Text;
            value = 0;
            if (String.IsNullOrEmpty(str))
            {
                MessageBox.Show("Не все коэффициенты заданы");
                return false;
            }

            var result = Int32.TryParse(str, out value);
            return result;
        }
    }
}
