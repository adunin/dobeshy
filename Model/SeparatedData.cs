﻿using System;
using System.Collections.Generic;

namespace Dobeshy.Model
{
    public class SeparatedData<T> : IDisposable
    {
        public List<KeyValuePair<List<T>, List<T>>> data { get; private set; }

        public SeparatedData()
        {
            data = new List<KeyValuePair<List<T>, List<T>>>();
            lastData = new List<T>();
        }

        public void Add(List<T> vector)
        {
            List<T> first = new List<T>();
            List<T> second = new List<T>();
            for (int i = 0; i < vector.Count; i++)
            {
                first.Add(vector[i]);
                i++;
                second.Add(vector[i]);
            }

            data.Add(new KeyValuePair<List<T>, List<T>>(first, second));
        }

        public void Add(List<T> up, List<T> down)
        {
            data.Add(new KeyValuePair<List<T>, List<T>>(up, down));
        }

        private List<T> lastData;

        public KeyValuePair<List<T>, List<T>> this[int index]
        {
            get{return data[index];} 
            set { data[index] = value; }
        }

        //public List<T> this[int index]
        //{
        //    get
        //    {
        //        if (lastIndex == index)
        //            return lastData;

        //        lastData = data[index].Key.Concat(data[index].Value).ToList();
        //        return lastData;
        //    }
        //    set
        //    {
        //        Add(value);
        //    }
        //}

        public void Dispose()
        {
            data.Clear();
            data = null;
            lastData.Clear();
            lastData = null;
        }

        public int Count{get { return data.Count; }}
    }
}
