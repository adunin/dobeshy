﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dobeshy.Model
{
    public class ConversationData<T>:IDisposable
    {
        private List<List<T>> data;

        private ConversationData()
        {
            this.data = new List<List<T>>();
        }

        public ConversationData(List<T> data)
        {
            this.data = new List<List<T>>();
            this.data.Add(data);
        }

        public List<T> this[int index]
        {
            get
            {
                if (index >= data.Count)
                    return null;
                return data[index];
            }
            set
            {
                if (index >= data.Count)
                    throw new ArgumentOutOfRangeException(string.Format("Данных по индексу {0} нет.", index));
                data[index] = value;
            }
        }

        public void Add(List<T> value)
        {
            data.Add(value);
        }

        public int Count
        {
            get { return data.Count; }
        }

        public void Dispose()
        {
            this.data.Clear();
        }
    }
}
