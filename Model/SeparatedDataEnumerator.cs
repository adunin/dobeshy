﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dobeshy.Model
{
    public class SeparatedDataEnumerator<T>
    {
        private KeyValuePair<List<T>, List<T>> data;
        private int size;
        private int curPos;

        public SeparatedDataEnumerator(KeyValuePair<List<T>, List<T>> data, int size)
        {
            this.data = data;
            this.size = size;
            this.curPos = 0;
        }

        public List<T> Current { get; private set; }
        public bool GetNext()
        {
            if (curPos >= size)
                return false;

            List<T> result = new List<T>();
            int len = size/2;

            for (int i = 0; i < len; i++)
            {
                result.Add(data.Key[i]);
                result.Add(data.Value[i]);
            }

            //for (int i = 0; i < len; i++)
            //    result.Add(data.Key[curPos + i]);
            //for (int i = 0; i < len; i++)
            //    result.Add(data.Value[curPos + i]);

            curPos += size;
            Current = result;
            return true;
        }
    }
}
