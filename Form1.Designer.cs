﻿namespace Dobeshy
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FileDialog = new System.Windows.Forms.OpenFileDialog();
            this.OpenButton = new System.Windows.Forms.Button();
            this.FileTextBox = new System.Windows.Forms.TextBox();
            this.OpenFileGroupBox = new System.Windows.Forms.GroupBox();
            this.StartButton = new System.Windows.Forms.Button();
            this.MatrixParamsGroupBox = new System.Windows.Forms.GroupBox();
            this.DecreamentButton = new System.Windows.Forms.Button();
            this.IncreamentButton = new System.Windows.Forms.Button();
            this.MatrixsizeTextBox = new System.Windows.Forms.TextBox();
            this.MatrixSizeLabel = new System.Windows.Forms.Label();
            this.IsUseIntCheckBox = new System.Windows.Forms.CheckBox();
            this.IsDefaultCheckBox = new System.Windows.Forms.CheckBox();
            this.C4TextBox = new System.Windows.Forms.TextBox();
            this.C4Label = new System.Windows.Forms.Label();
            this.C3TextBox = new System.Windows.Forms.TextBox();
            this.C3Label = new System.Windows.Forms.Label();
            this.C2TextBox = new System.Windows.Forms.TextBox();
            this.C2Label = new System.Windows.Forms.Label();
            this.C1TextBox = new System.Windows.Forms.TextBox();
            this.C1Label = new System.Windows.Forms.Label();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ProcessProgressBar = new System.Windows.Forms.ProgressBar();
            this.ModuleTextBox = new System.Windows.Forms.TextBox();
            this.ModulLabel = new System.Windows.Forms.Label();
            this.OpenFileGroupBox.SuspendLayout();
            this.MatrixParamsGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // FileDialog
            // 
            this.FileDialog.FileName = "FileDialog";
            // 
            // OpenButton
            // 
            this.OpenButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenButton.Location = new System.Drawing.Point(572, 16);
            this.OpenButton.Name = "OpenButton";
            this.OpenButton.Size = new System.Drawing.Size(75, 23);
            this.OpenButton.TabIndex = 0;
            this.OpenButton.Text = "Открыть";
            this.OpenButton.UseVisualStyleBackColor = true;
            // 
            // FileTextBox
            // 
            this.FileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileTextBox.Location = new System.Drawing.Point(9, 19);
            this.FileTextBox.Name = "FileTextBox";
            this.FileTextBox.ReadOnly = true;
            this.FileTextBox.Size = new System.Drawing.Size(548, 20);
            this.FileTextBox.TabIndex = 1;
            // 
            // OpenFileGroupBox
            // 
            this.OpenFileGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OpenFileGroupBox.Controls.Add(this.FileTextBox);
            this.OpenFileGroupBox.Controls.Add(this.OpenButton);
            this.OpenFileGroupBox.Location = new System.Drawing.Point(3, 12);
            this.OpenFileGroupBox.Name = "OpenFileGroupBox";
            this.OpenFileGroupBox.Size = new System.Drawing.Size(653, 52);
            this.OpenFileGroupBox.TabIndex = 2;
            this.OpenFileGroupBox.TabStop = false;
            this.OpenFileGroupBox.Text = "Выбрать файл";
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(575, 83);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(75, 23);
            this.StartButton.TabIndex = 4;
            this.StartButton.Text = "Пуск";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // MatrixParamsGroupBox
            // 
            this.MatrixParamsGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MatrixParamsGroupBox.Controls.Add(this.ModulLabel);
            this.MatrixParamsGroupBox.Controls.Add(this.ModuleTextBox);
            this.MatrixParamsGroupBox.Controls.Add(this.DecreamentButton);
            this.MatrixParamsGroupBox.Controls.Add(this.IncreamentButton);
            this.MatrixParamsGroupBox.Controls.Add(this.MatrixsizeTextBox);
            this.MatrixParamsGroupBox.Controls.Add(this.MatrixSizeLabel);
            this.MatrixParamsGroupBox.Controls.Add(this.IsUseIntCheckBox);
            this.MatrixParamsGroupBox.Controls.Add(this.IsDefaultCheckBox);
            this.MatrixParamsGroupBox.Controls.Add(this.C4TextBox);
            this.MatrixParamsGroupBox.Controls.Add(this.C4Label);
            this.MatrixParamsGroupBox.Controls.Add(this.C3TextBox);
            this.MatrixParamsGroupBox.Controls.Add(this.C3Label);
            this.MatrixParamsGroupBox.Controls.Add(this.C2TextBox);
            this.MatrixParamsGroupBox.Controls.Add(this.C2Label);
            this.MatrixParamsGroupBox.Controls.Add(this.C1TextBox);
            this.MatrixParamsGroupBox.Controls.Add(this.C1Label);
            this.MatrixParamsGroupBox.Location = new System.Drawing.Point(3, 70);
            this.MatrixParamsGroupBox.Name = "MatrixParamsGroupBox";
            this.MatrixParamsGroupBox.Size = new System.Drawing.Size(243, 234);
            this.MatrixParamsGroupBox.TabIndex = 5;
            this.MatrixParamsGroupBox.TabStop = false;
            this.MatrixParamsGroupBox.Text = "Параметры матрицы";
            // 
            // DecreamentButton
            // 
            this.DecreamentButton.Location = new System.Drawing.Point(130, 13);
            this.DecreamentButton.Name = "DecreamentButton";
            this.DecreamentButton.Size = new System.Drawing.Size(22, 23);
            this.DecreamentButton.TabIndex = 7;
            this.DecreamentButton.Text = "-";
            this.DecreamentButton.UseVisualStyleBackColor = true;
            this.DecreamentButton.Click += new System.EventHandler(this.DecreamentButton_Click);
            // 
            // IncreamentButton
            // 
            this.IncreamentButton.Location = new System.Drawing.Point(205, 13);
            this.IncreamentButton.Name = "IncreamentButton";
            this.IncreamentButton.Size = new System.Drawing.Size(22, 23);
            this.IncreamentButton.TabIndex = 7;
            this.IncreamentButton.Text = "+";
            this.IncreamentButton.UseVisualStyleBackColor = true;
            this.IncreamentButton.Click += new System.EventHandler(this.IncreamentButton_Click);
            // 
            // MatrixsizeTextBox
            // 
            this.MatrixsizeTextBox.Location = new System.Drawing.Point(151, 15);
            this.MatrixsizeTextBox.Name = "MatrixsizeTextBox";
            this.MatrixsizeTextBox.ReadOnly = true;
            this.MatrixsizeTextBox.Size = new System.Drawing.Size(59, 20);
            this.MatrixsizeTextBox.TabIndex = 6;
            this.MatrixsizeTextBox.Text = "4";
            // 
            // MatrixSizeLabel
            // 
            this.MatrixSizeLabel.AutoSize = true;
            this.MatrixSizeLabel.Location = new System.Drawing.Point(9, 17);
            this.MatrixSizeLabel.Name = "MatrixSizeLabel";
            this.MatrixSizeLabel.Size = new System.Drawing.Size(100, 13);
            this.MatrixSizeLabel.TabIndex = 5;
            this.MatrixSizeLabel.Text = "Размер матрицы: ";
            // 
            // IsUseIntCheckBox
            // 
            this.IsUseIntCheckBox.AutoSize = true;
            this.IsUseIntCheckBox.Location = new System.Drawing.Point(9, 48);
            this.IsUseIntCheckBox.Name = "IsUseIntCheckBox";
            this.IsUseIntCheckBox.Size = new System.Drawing.Size(166, 17);
            this.IsUseIntCheckBox.TabIndex = 2;
            this.IsUseIntCheckBox.Text = "Использовать целые числа";
            this.IsUseIntCheckBox.UseVisualStyleBackColor = true;
            this.IsUseIntCheckBox.CheckedChanged += new System.EventHandler(this.UseIntCheckChanged);
            // 
            // IsDefaultCheckBox
            // 
            this.IsDefaultCheckBox.AutoSize = true;
            this.IsDefaultCheckBox.Location = new System.Drawing.Point(9, 96);
            this.IsDefaultCheckBox.Name = "IsDefaultCheckBox";
            this.IsDefaultCheckBox.Size = new System.Drawing.Size(223, 17);
            this.IsDefaultCheckBox.TabIndex = 2;
            this.IsDefaultCheckBox.Text = "Использовать значения по умолчанию";
            this.IsDefaultCheckBox.UseVisualStyleBackColor = true;
            this.IsDefaultCheckBox.CheckedChanged += new System.EventHandler(this.UseDefaultCheckChanged);
            // 
            // C4TextBox
            // 
            this.C4TextBox.Location = new System.Drawing.Point(130, 180);
            this.C4TextBox.Name = "C4TextBox";
            this.C4TextBox.Size = new System.Drawing.Size(100, 20);
            this.C4TextBox.TabIndex = 1;
            // 
            // C4Label
            // 
            this.C4Label.AutoSize = true;
            this.C4Label.Location = new System.Drawing.Point(134, 163);
            this.C4Label.Name = "C4Label";
            this.C4Label.Size = new System.Drawing.Size(93, 13);
            this.C4Label.TabIndex = 0;
            this.C4Label.Text = "Коэффициент C4";
            // 
            // C3TextBox
            // 
            this.C3TextBox.Location = new System.Drawing.Point(130, 137);
            this.C3TextBox.Name = "C3TextBox";
            this.C3TextBox.Size = new System.Drawing.Size(100, 20);
            this.C3TextBox.TabIndex = 1;
            // 
            // C3Label
            // 
            this.C3Label.AutoSize = true;
            this.C3Label.Location = new System.Drawing.Point(134, 119);
            this.C3Label.Name = "C3Label";
            this.C3Label.Size = new System.Drawing.Size(93, 13);
            this.C3Label.TabIndex = 0;
            this.C3Label.Text = "Коэффициент C3";
            // 
            // C2TextBox
            // 
            this.C2TextBox.Location = new System.Drawing.Point(9, 181);
            this.C2TextBox.Name = "C2TextBox";
            this.C2TextBox.Size = new System.Drawing.Size(100, 20);
            this.C2TextBox.TabIndex = 1;
            // 
            // C2Label
            // 
            this.C2Label.AutoSize = true;
            this.C2Label.Location = new System.Drawing.Point(9, 164);
            this.C2Label.Name = "C2Label";
            this.C2Label.Size = new System.Drawing.Size(93, 13);
            this.C2Label.TabIndex = 0;
            this.C2Label.Text = "Коэффициент C2";
            // 
            // C1TextBox
            // 
            this.C1TextBox.Location = new System.Drawing.Point(9, 136);
            this.C1TextBox.Name = "C1TextBox";
            this.C1TextBox.Size = new System.Drawing.Size(100, 20);
            this.C1TextBox.TabIndex = 1;
            // 
            // C1Label
            // 
            this.C1Label.AutoSize = true;
            this.C1Label.Location = new System.Drawing.Point(9, 119);
            this.C1Label.Name = "C1Label";
            this.C1Label.Size = new System.Drawing.Size(93, 13);
            this.C1Label.TabIndex = 0;
            this.C1Label.Text = "Коэффициент C1";
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(575, 112);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 4;
            this.CancelButton.Text = "Отмена";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ProcessProgressBar
            // 
            this.ProcessProgressBar.Location = new System.Drawing.Point(252, 281);
            this.ProcessProgressBar.Name = "ProcessProgressBar";
            this.ProcessProgressBar.Size = new System.Drawing.Size(398, 23);
            this.ProcessProgressBar.TabIndex = 6;
            // 
            // ModuleTextBox
            // 
            this.ModuleTextBox.Location = new System.Drawing.Point(130, 69);
            this.ModuleTextBox.Name = "ModuleTextBox";
            this.ModuleTextBox.Size = new System.Drawing.Size(100, 20);
            this.ModuleTextBox.TabIndex = 8;
            // 
            // ModulLabel
            // 
            this.ModulLabel.AutoSize = true;
            this.ModulLabel.Location = new System.Drawing.Point(9, 72);
            this.ModulLabel.Name = "ModulLabel";
            this.ModulLabel.Size = new System.Drawing.Size(90, 13);
            this.ModulLabel.TabIndex = 9;
            this.ModulLabel.Text = "Основание поля";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 316);
            this.Controls.Add(this.ProcessProgressBar);
            this.Controls.Add(this.MatrixParamsGroupBox);
            this.Controls.Add(this.OpenFileGroupBox);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.StartButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.OpenFileGroupBox.ResumeLayout(false);
            this.OpenFileGroupBox.PerformLayout();
            this.MatrixParamsGroupBox.ResumeLayout(false);
            this.MatrixParamsGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog FileDialog;
        private System.Windows.Forms.Button OpenButton;
        private System.Windows.Forms.TextBox FileTextBox;
        private System.Windows.Forms.GroupBox OpenFileGroupBox;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.GroupBox MatrixParamsGroupBox;
        private System.Windows.Forms.TextBox C4TextBox;
        private System.Windows.Forms.Label C4Label;
        private System.Windows.Forms.TextBox C3TextBox;
        private System.Windows.Forms.Label C3Label;
        private System.Windows.Forms.TextBox C2TextBox;
        private System.Windows.Forms.Label C2Label;
        private System.Windows.Forms.TextBox C1TextBox;
        private System.Windows.Forms.Label C1Label;
        private System.Windows.Forms.CheckBox IsDefaultCheckBox;
        private System.Windows.Forms.Label MatrixSizeLabel;
        private System.Windows.Forms.Button DecreamentButton;
        private System.Windows.Forms.Button IncreamentButton;
        private System.Windows.Forms.TextBox MatrixsizeTextBox;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.ProgressBar ProcessProgressBar;
        private System.Windows.Forms.CheckBox IsUseIntCheckBox;
        private System.Windows.Forms.Label ModulLabel;
        private System.Windows.Forms.TextBox ModuleTextBox;
    }
}

